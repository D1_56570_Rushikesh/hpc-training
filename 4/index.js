function myFunction() {
  var x = document.getElementById("HPC-todo");
  var y = document.getElementById("v-pills-hpc");
  const elements = document.querySelectorAll(".tab-pane");
  if (y.classList.contains("active")) {
  } else {
    elements.forEach((box) => {
      box.classList.remove("active");
    });
    y.classList.add("show", "active");
  }
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function myFunction2() {
  var x = document.getElementById("MPI-todo");
  var y = document.getElementById("v-pills-pro");
  const elements = document.querySelectorAll(".tab-pane");
  if (y.classList.contains("active")) {
  } else {
    elements.forEach((box) => {
      box.classList.remove("active");
    });
    y.classList.add("show", "active");
  }
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

// const boxes = document.querySelectorAll('.box');

function playVideo() {
  const hpc = document.getElementsByClassName("hpc");
  hpc[0].style.display = "none";
  const video = document.getElementById("hpc-video");
  video.style.display = "block";
  const pdf = document.getElementById("hpc-pdf");
  pdf.style.display = "none";
  const ppt = document.getElementById("hpc-ppt");
  ppt.style.display = "none";
}

function openPDF() {
  const hpc = document.getElementsByClassName("hpc");
  hpc[0].style.display = "none";
  const video = document.getElementById("hpc-video");
  video.style.display = "none";
  const pdf = document.getElementById("hpc-pdf");
  pdf.style.display = "block";
}

// function openPPT() {
//   const hpc = document.getElementsByClassName("hpc");
//   hpc[0].style.display = "none";
//   const video = document.getElementById("hpc-video");
//   video.style.display = "none";
//   const pdf = document.getElementById("hpc-pdf");
//   pdf.style.display = "none";
//   const ppt = document.getElementById("hpc-ppt");
//   ppt.style.display = "block";
// }
